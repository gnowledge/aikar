**ICARE Website**
is a platform to connect NGOs to donors and vice versa through a moderated system. 
It allows donors to make all kind of donations.
Features of the website:	
- A platform to connect NGOs to donors and vice versa.
- The system has three modules namely, moderator, donor and NGO.
- Moderator can login using credentials and manage the account request by NGO by approving or rejecting it. Approval will be done after verifying the documents uploaded by the NGO.
- Moderator will get the report of NGOs that receive donations.
- NGOs can register and raise requests by uploading documents.  NGO in need can raise request to the registered donors. They can view the previous donations as well.
- Donors can add pictures, description, add tags to their donations.
- Items are given tags to facilitate easier search.
- Using an in-built chat system, NGO’s can communicate with donors via our portal and vice versa.
- Latest Status of each transaction will be visible 



